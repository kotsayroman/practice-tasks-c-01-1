using System;
using System.Collections;

namespace Task3
{
	class Vector
	{
		private int[] _vector;
		private int _begin;
		private int _end;

		public Vector(int[] vector = null)
		{			
			_begin = 0;
			_end = vector.Length;
		
			_vector = new int[vector.Length];
			for(int i = 0; i < vector.Length; i++)
			{
				_vector[i] = vector[i];
			}
		}

		public Vector(int begin = 0, int end = 0, int initValue = new int())
		{
			if(begin > end)
			{
				throw new Exception("End of vector can't be less than begin.");
			}
			
			_begin = begin;
			_end = end;
			
			_vector = new int[_end - _begin];
			
			for(int i = 0; i < _vector.Length; i++)
			{
				_vector[i] = initValue;
			}
		}

		public Vector(Vector vector)
		{
			_begin = vector._begin;
			_end = vector._end;
			
			_vector = new int[_end - _begin];
			
			for(int i = 0; i < _vector.Length; i++)
			{
				_vector[i] = vector._vector[i];
			}
		}

		public IEnumerator GetEnumerator()
		{
			foreach(int v in _vector)
			{
				yield return v;
			}
		}

		public override bool Equals(Object obj)
	    {
	        if (obj == null)
	        {
	            return false;
	        }
	        
	        Vector vector = obj as Vector;
	        
	        if ((Object)vector == null)
	        {
	            return false;
	        }

	        return (this == vector);
	    }

		public override int GetHashCode()
    	{
    		int result = _vector[0];

    		for(int i = 1; i < _end - _begin; i++)
    		{
    			result ^= _vector[i];
    		}

        	return result;
	    }

		public int this[int i]
		{			
			get
			{
				if((i < _begin) || (i >= _end))
				{
					throw new IndexOutOfRangeException("Index out of range!");
				}

				return _vector[i - _begin];
			}
			
			set
			{
				if((i < _begin) || (i >= _end))
				{
					throw new IndexOutOfRangeException("Index out of range!");
				}
				_vector[i - _begin] = value;
			}
		}

		public Vector Add(Vector vector)
		{
			if((_begin != vector._begin) || (_end != vector._end))
			{
				throw new Exception("Vectors haven't equel length.");
			}
			
			for(int i = 0; i < _end - _begin; i++)
			{
				_vector[i] += vector._vector[i];
			}
			
			return this;
		}

		public Vector Subtract(Vector vector)
		{
			if((_begin != vector._begin) || (_end != vector._end))
			{
				throw new Exception("Vectors haven't equel length.");
			}
			
			for(int i = 0; i < _end - _begin; i++)
			{
				_vector[i] -= vector._vector[i];
			}
			
			return this;
		}

		public static Vector operator+(Vector vector1, Vector vector2)
		{
			if((vector1._begin != vector2._begin) || (vector1._end != vector2._end))
			{
				throw new Exception("Vectors haven't equel length.");
			}

			Vector result = new Vector(vector1._begin, vector1._end);

			for(int i = 0; i < result._end - result._begin; i++)
			{
				result._vector[i] = vector1._vector[i] + vector2._vector[i];
			}

			return result;
		}

		public static Vector operator-(Vector vector1, Vector vector2)
		{
			if((vector1._begin != vector2._begin) || (vector1._end != vector2._end))
			{
				throw new Exception("Vectors haven't equel length.");
			}

			Vector result = new Vector(vector1._begin, vector1._end);

			for(int i = 0; i < result._end - result._begin; i++)
			{
				result._vector[i] = vector1._vector[i] - vector2._vector[i];
			}
			
			return result;
		}

		public static bool operator==(Vector vector1, Vector vector2)
		{
			if((vector1._begin != vector2._begin) || (vector1._end != vector2._end))
			{
				return false;
			}

			for(int i = 0; i < vector1._end - vector1._begin; i++)
			{
				if(vector1._vector[i] != vector2._vector[i])
				{
					return false;
				}
			}

			return true;
		}

		public static bool operator!=(Vector vector1, Vector vector2)
		{
			return !(vector1 == vector2);
		}

		public static bool operator>(Vector vector1, Vector vector2)
		{
			if((vector1._begin != vector2._begin) || (vector1._end != vector2._end))
			{
				return false;
			}

			for(int i = 0; i < vector1._end - vector1._begin; i++)
			{
				if(vector1._vector[i] < vector2._vector[i])
				{
					return false;
				}
			}

			return true;	
		}

		public static bool operator<(Vector vector1, Vector vector2)
		{
			if((vector1._begin != vector2._begin) || (vector1._end != vector2._end))
			{
				return false;
			}

			for(int i = 0; i < vector1._end - vector1._begin; i++)
			{
				if(vector1._vector[i] > vector2._vector[i])
				{
					return false;
				}
			}

			return true;	
		}

		public Vector MultiplyByScalar(int scalar)
		{
			for(int i = 0; i < _end - _begin; i++)
			{
				_vector[i] *= scalar;
			}

			return this;
		}
	}

	class Program
	{
		static void PrintVector(Vector v)
		{
			foreach(var a in v)
			{
				Console.Write("{0} ", a);
			}

			Console.WriteLine();
		}
		static void Main()
		{
			try
			{
				Vector v1 = new Vector(-1, 4, 6);
				Vector v2 = new Vector(4, 7);
				Vector v3 = new Vector(v1);
			
				Console.Write("v1 = ");
				PrintVector(v1);
				Console.Write("v2 = ");
				PrintVector(v2);
				Console.Write("v3 = ");
				PrintVector(v3);
				Console.WriteLine();
			
				Console.WriteLine("Is v1 == v2? {0}", v1 == v2);
				Console.WriteLine("Is v1 == v3? {0}", v1 == v3);
				Console.WriteLine();
			
				Console.WriteLine("Is v1 equel v2? {0}", v1.Equals(v2));
				Console.WriteLine("Is v1 equel v3? {0}", v1.Equals(v3));
				Console.WriteLine();
				
				Console.Write("v3 = ");
				PrintVector(v3);
				Console.WriteLine();
				Console.Write("v3 after multiply by 2 = ");
				PrintVector(v3.MultiplyByScalar(2));
				Console.WriteLine();
			
				Console.WriteLine("Is v1 > v3? {0}", v1 > v3);
				Console.WriteLine("Is v1 < v3? {0}", v1 < v3);
				Console.WriteLine();
			

				Console.Write("v3 = ");
				PrintVector(v3);

				for(int i = -1; i < 4; i++)
				{
					if(i % 2 == 0)
					{
						v3[i] += 3;
					}
					else
					{
						v3[i] -= 3;
					}
				}
							
				Console.Write("v3 after change with [] = ");
				PrintVector(v3);
			
				Console.Write("v1 = ");
				PrintVector(v1);
				Console.Write("v3 = ");
				PrintVector(v3);
				Console.Write("v1 + v3 = ");
				PrintVector(v1 + v3);
				Console.WriteLine();

				Console.Write("v1 = ");
				PrintVector(v1);
				Console.Write("v3 = ");
				PrintVector(v3);
				Console.Write("v1 - v3 = ");
				PrintVector(v1 - v3);
				Console.WriteLine();

				Console.Write("v1 = ");
				PrintVector(v1);
				Console.Write("v3 = ");
				PrintVector(v3);
				Console.Write("v1.Add(v3) = ");
				PrintVector(v1.Add(v3));

				Console.WriteLine();
				Console.Write("v1 = ");
				PrintVector(v1);
				Console.Write("v3 = ");
				PrintVector(v3);
				Console.Write("v1.Subtract(v3) = ");
				PrintVector(v1.Subtract(v3));
				Console.WriteLine();

				v2[0] = 1;
			}
			catch(IndexOutOfRangeException e)
			{
				Console.WriteLine(e);
			}
			catch(Exception e)
			{
				Console.WriteLine(e);
			}

		}
	}
}